# What is it?

A wrapper we developed so we can re-use the logic of having a centralized place
that handles multiple queues, monitors tasks, schedules them, re-schedules them,
handles errors, re-try on error, and have management commands to allow cron-jobs.

## Installation

#### by using `poetry` (as a env management tool we are using right now)

    poetry add git+https://bitbucket.org/gramsoft/taskqueue.git

#### settings file:

   - add to ``INSTALLED_APPS`` as ``taskqueue``
   - `TASKQUEUE_BROKER_CONFIG = {"url": "...", "options": {} }`  
       - where the url should match the ``broker_transport_url`` setting for celery,
   and options is a dict matching `broker_transport_options` setting for celery.
       - default for `url` is `amqp://` 
       - default for `options` when `DEBUG=True` is `{}`. Otherwise:
       ```
       {
         "polling_interval": 30.0, 
         "visibility_timeout": 1800,
         "wait_time_seconds": 20,
       }
       ```
   - `TASKQUEUE_RESULT_BACKEND = "..."`
       - the result backend as required by celery's `result_backend`
       - defaults to `redis://localhost/0`
   - `TASKQUEUE_QUEUES_MAPPING = {"default": "default_queue", ...}`
       - a mapping of the queues used for this project
       - at-least - the `"default"` is required
       - multiple queue "names" can map to the same real-queue so `"default_queue"` can be used a the "default" and as the "pipeline_queue"
       - you can use this as a `target_queue` (as described below) by using `settings.TASKQUEUE_QUEUES_MAPPING.get("queue_name")`
   - `TASKQUEUE_USE_REDIS = True`
       - Whether to use redis to check for "flags" when deciding on running a task. A task checks that it's not in the ignored key before running.
       - Disabling (`False`) this feature means that the task always run without checking for the flag
       - Default: `True`
   - `TASKQUEUE_REDIS_ALIAS = "default"`
       - the redis alias to connect to (using `django_redis`)
       - Default: `default`
   - `TASKQUEUE_RESCHEDULE_FAILED_DAYS = 1`
       - when a task failed, reschedule it to the this many days later
       - Default: 1 (1 day)
   - `TASKQUEUE_NOTIFICATION_MAIL_PREFIX = ''`
       - A prefix that will be used for the email notifying about unresponsive tasks.
       - This should be used to add the application name and environment to the notification email.
   - `TASKQUEUE_NOTIFICATION_UNRESPONSIVE_TIME = 5`
       - The grace time (in minutes) for a task, until it is considered unresponsive.
       - The default is 5 minutes - if a task was scheduled to 5 mins ago and hasn't finished running yet,
       - it is considered unresponsive.
#### Connect celery with this package configurations

   in `celeryapp` or wherever you decide is the starting point for celery use this:
   `app.config_from_object("taskqueue.config")`
 

## API

#### `class MonitoredTask(celery.Task)`
   
 This is the base class for all celery tasks in the app.
    We added monitoring abilities to the base task,
    so we will be able to analyze the execution times of different tasks,
    the success rates, which tasks are currently in the queue and be able to stop tasks that we don't want to run.
    In order to be able to monitor the tasks, we created the TaskMonitorLog model,
    and overriden some of the celery task functions so we can update the task's status in the database.
    
 In the `apply_async` function we first create a record for this task in the log, and mark the task as queued.
    In the `apply_async` we can create or update an existing log record,
    since we might want to resend some tasks to the queue.
    For example, a task that was marked as "ignored" or "failed", can be resent to the queue with the same task id.
    
 The `__call__` function allows us to check whether this task should be executed or ignored,
    and updates the task's status to "ignored" or "running".
    
 When the task is finished, we update the task's status to "failed" or "completed",
    and we update the task's execution times.
    
 `on_success` - if this is a recurring scheduled task we reschedule for the next run by
    calculating a task's `cron_schedule`, otherwise we remove it and save its running data as a `CompletedTask`
    
 `on_failure` - we create or update the task (create if it originated from a simple `delay` or `apply_async`
    call and not from the `ScheduledTask` mechanism), and reschedule it for 24 hours from now
    
 This should be used for `shared_task` in celery like so:
    
    `@shared_task(name="path.name.to.task", serializer="json", base=MonitoredTask)`


#### create_scheduled_task

 ```
 taskqueue.utils.create_scheduled_task(
     task_name: str,
     next_run: datetime.datetime,
     cron_schedule: Optional[str]=None,
     task_args: Optional[Union[Tuple[Any], Dict[str, Any]]]=None,
     failed_attempts: int=0,
     obj_id: str=None,
     target_queue: Optional[str]=None,
 )
 ```
    
  create a new scheduled task:
    - `task_name` - a string representing the task name to run (full path)
    - `next_run` - the date-time to schedule the task for
    - `cron_schdule` - a string in crontab format for repetitive tasks
    - `task_args` - A tuple, list or dict - parameters that will be sent as is to the task
    - `failed_attempts` - the number of failed attempts, if any
    - `obj_id` - the object this task is done on. This helps if rescheduling the same task on the same object. In the 
        case where the task name and obj_id are the same, and the existing task is not marked for queue
        or queued already - we only update the existing task, instead of scheduling a new one
    - `target_queue` - a queue name from `settings.TASKQUEUE_QUEUES_MAPPING` (note: the value, not the key)
   





#### update_scheduled_task

```
taskqueue.utils.update_scheduled_task(
    task: ScheduledTask, 
    next_run: Optional[str]=None, 
    cron_schedule: Optional[str]=None, 
    is_failed_task: Optional[bool]=None, 
    target_queue: Optional[str]=None
)
```
  
  updates an existing `ScheduledTask`
  - `task` - the ScheduledTask instance to update
  - `next_run` - the time to reschedule the `task` to
  - `cron_schedule` - updates the cron schedule for the recurring task
  - `is_failed_task` - whether this method called because the `task` has failed. 
     in that case the task is marked as FAILED and rescheduled for `settings.TASKQUEUE_RESCHEDULE_FAILED_DAYS` days from now
  - `target_queue` - a queue name from `settings.TASKQUEUE_QUEUES_MAPPING` (note: the value, not the key)



### remove_scheduled_task

 ```
  taskqueue.utils.remove_scheduled_task(
       task_id: Optional[int]=None, 
       task_name: Optional[str]=None,
       args: Optional[Union[Tuple[Any], Dict[str, Any]]]=None
  )
  ```
  
  removes a task from the schedule.
  * `task_id` - the `ScheduledTask` id to remove
  * `task_name` - the task name
  * `args` - the arguments used for this task
  
  either `task_id` or (`task_name` and `args`) should be provided as this is the way the task
  is found for removal. 
  
  Normally there's no reason to cancel a `ScheduledTask` but this is here
  if needed
  
## Management commands
* `notify_unresponsive_tasks (WORKS, but normally not used)`
   
   Checks if there are any tasks that their scheduled time has passed but are not
   queued yet. 
   
   An unresponsive task is a task that was scheduled to run more than 15 mins ago,
        is marked for queue and wasn't moved to the completed tasks table.
        In this case we will resend this task to the queue.
        
* `queue_scheduled_tasks`
  
   Gets all tasks that needs to be enqueued and **mark them for queue** in an atomic transaction
   (so if this is run again before the tasks were actually queued, they won't be queued twice)
   and then queues said tasks.
  
  

 
 


