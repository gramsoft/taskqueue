IGNORED_TASKS_SET_NAME = "tasks_to_ignore"


class TASK_STATUSES:
    QUEUED = "queued"
    IGNORED = "ignored"
    RUNNING = "running"
    COMPLETED = "completed"
    FAILED = "failed"
    collection = {QUEUED: 0, IGNORED: 1, RUNNING: 2, COMPLETED: 3, FAILED: 4}

    @classmethod
    def get(cls, k):
        return cls.collection.get(k)

    @classmethod
    def rev_collection(cls):
        return {v: k for k, v in cls.collection.items()}

    @classmethod
    def as_tuple(cls):
        return tuple((v, k) for k, v in cls.collection.items())
