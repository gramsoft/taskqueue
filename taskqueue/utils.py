from taskqueue.models import ScheduledTask, CompletedTask, TaskMonitorLog
from croniter import croniter
from datetime import datetime, timedelta
import importlib
import json
from django.db import transaction
from celery import Task, utils
import time
from django.conf import settings
from django.utils import timezone
from django.core.mail import mail_admins
from celery.exceptions import Ignore
from taskqueue.consts import IGNORED_TASKS_SET_NAME, TASK_STATUSES

use_redis = getattr(settings, "TASKQUEUE_USE_REDIS", False)


def get_redis_connection():
    if use_redis:
        from django_redis import get_redis_connection
        redis_connection_alias = getattr(settings, "TASKQUEUE_REDIS_ALIAS", "default")
        return get_redis_connection(redis_connection_alias)
    return object()


def _convert_args_to_new_format(task_args):
    # Convert old format to new format
    task_args_conversion = {"args": [], "kwargs": {}}

    if task_args:
        if isinstance(task_args, dict):
            if "args" in task_args.keys() and "kwargs" in task_args.keys():
                # This is the new format, use as-is
                task_args_conversion = task_args
            else:
                task_args_conversion["kwargs"] = task_args
        else:
            task_args_conversion["args"] = task_args

    return task_args_conversion


class MonitoredTask(Task):
    """
    This is the base class for all celery tasks in the app.
    We added monitoring abilities to the base task,
    so we will be able to analyze the execution times of different tasks,
    the success rates, which tasks are currently in the queue and be able to stop tasks that we don't want to run.
    In order to be able to monitor the tasks, we created the TaskMonitorLog model,
    and overriden some of the celery task functions so we can update the task's status in the database.
    
    In the apply_async function we first create a record for this task in the log, and mark the task as queued.
    In the apply_async we can create or update an existing log record,
    since we might want to resend some tasks to the queue.
    For example, a task that was marked as "ignored" or "failed", can be resent to the queue with the same task id.
    
    The __call__ function allows us to check whether this task should be executed or ignored,
    and updates the task's status to "ignored" or "running".
    
    When the task is finished, we update the task's status to "failed" or "completed",
    and we update the task's execution times.
    """

    def __call__(self, *args, **kwargs):
        # If task has request.id, this means it wasn't called directly, but through celery
        if self.request.id:
            self.start_time = timezone.now()

            # Check if this kind of tasks should currently be ignored.
            if use_redis and get_redis_connection().sismember(IGNORED_TASKS_SET_NAME, self.name):
                self.ignore_task(*args, **kwargs)
                raise Ignore()
            else:
                # Mark the task as running
                TaskMonitorLog.objects.filter(uuid=self.request.id).update(
                    status=TASK_STATUSES.get(TASK_STATUSES.RUNNING),
                    start_time=self.start_time.timestamp(),
                )

        return Task.__call__(self, *args, **kwargs)

    def apply_async(
        self,
        args=None,
        kwargs=None,
        task_id=None,
        producer=None,
        link=None,
        link_error=None,
        shadow=None,
        **options,
    ):
        # We are generating a uuid so there won't be a concurrency issue.
        # We are waiting for the apply_async to finish, in order to create a new record in the monitor log.
        # Meanwhile, a worker might already receive this message and start the task, so the worker won't
        # be able to update the log record, because it wasn't created yet.
        # So we want to first create the log record and then call apply_async.
        # For that, we need to have a task_id, so we generate it by ourselves, instead of letting
        # celery do it.
        if not task_id:
            task_id = utils.uuid()

        TaskMonitorLog.objects.update_or_create(
            uuid=task_id,
            defaults={
                "task_name": self.name,
                "task_args": {"args": args or [], "kwargs": kwargs or {}},
                "status": TASK_STATUSES.get(TASK_STATUSES.QUEUED),
            },
        )

        return Task.apply_async(
            self,
            args=args,
            kwargs=kwargs,
            task_id=task_id,
            producer=producer,
            link=link,
            link_error=link_error,
            shadow=shadow,
            **options,
        )

    def ignore_task(self, *args, **kwargs):
        TaskMonitorLog.objects.filter(uuid=self.request.id).update(
            status=TASK_STATUSES.get(TASK_STATUSES.IGNORED)
        )

    def post_finish(self, retval, task_id, args, kwargs, status):
        from django.db.models import F

        end_time = timezone.now().timestamp()
        TaskMonitorLog.objects.filter(uuid=task_id).update(
            end_time=end_time, status=status, execution_time=end_time - F("start_time")
        )

    # This event fires up when a task ends successfully
    def on_success(self, retval, task_id, args, kwargs):
        schedule_id = kwargs.pop("schedule_id", None)

        # Checking if the task that just finished was a scheduled task. If so, rescheduling it in case it's a
        # re-occurring task
        if schedule_id:
            sct = ScheduledTask.objects.filter(id=schedule_id).first()

            if sct:
                if sct.cron_schedule:
                    reschedule_task(sct, "SUCCESS")
                else:
                    CompletedTask.objects.create(
                        task_name=sct.task_name,
                        task_args=_convert_args_to_new_format(sct.task_args or sct.args),
                        next_run=sct.next_run,
                        cron_schedule=sct.cron_schedule,
                        mark_for_queue=sct.mark_for_queue,
                        queued=sct.queued,
                        result=sct.result,
                        failed_attempts=sct.failed_attempts,
                        obj_id=sct.obj_id,
                    )
                    sct.delete()

        Task.on_success(self, retval, task_id, args, kwargs)
        self.post_finish(
            retval, task_id, args, kwargs, TASK_STATUSES.get(TASK_STATUSES.COMPLETED)
        )

    # This event fires up when a task fails
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        schedule_id = kwargs.pop("schedule_id", None)

        # Checking if the task is already scheduled by the scheduler.
        # If not, creating a new task, otherwise re-scheduling the existing task
        if schedule_id:
            update_scheduled_task(schedule_id, is_failed_task=True)
        else:
            task_args = {"args": args or [], "kwargs": kwargs or {}}

            create_scheduled_task(
                self.name,
                timezone.now() + timedelta(days=getattr(settings, "TASKQUEUE_RESCHEDULE_FAILED_DAYS", 1)),
                task_args=task_args,
                failed_attempts=1,
                obj_id=str(time.time()),
            )

        if not settings.DEBUG:
            message = f"""
                At: {timezone.now()}, 
                Task had error, will retry in 24 hours, 
                The following exception occurred in task {self.name}\n{exc}\nTraceback:{einfo}
            """
            mail_admins(
                subject=f"An error occurred in task {self.name}",
                message=message,
                html_message=message
            )

        Task.on_failure(self, exc, task_id, args, kwargs, einfo)
        self.post_finish(
            None, task_id, args, kwargs, TASK_STATUSES.get(TASK_STATUSES.FAILED)
        )


def create_scheduled_task(
    task_name,
    next_run,
    cron_schedule=None,
    task_args=None,
    failed_attempts=0,
    obj_id=None,
    target_queue=None,
):
    task = ScheduledTask.objects.filter(
        task_name=task_name, obj_id=str(obj_id) if obj_id else None, mark_for_queue=False, queued=False
    ).first()

    task_args = _convert_args_to_new_format(task_args)

    # Checking if this task already exists, if so - only updating the schedule time, otherwise, creating a new task
    if task is None:
        task = ScheduledTask.objects.create(
            task_name=task_name,
            obj_id=str(obj_id) if obj_id else None,
            task_args=task_args,
            next_run=next_run,
            cron_schedule=cron_schedule,
            mark_for_queue=False,
            queued=False,
            result=None,
            failed_attempts=failed_attempts,
            target_queue=target_queue,
        )
    else:
        update_scheduled_task(task, next_run, cron_schedule, target_queue=target_queue)

    return task


def update_scheduled_task(
    task, next_run=None, cron_schedule=None, is_failed_task=None, target_queue=None
):
    if not isinstance(task, ScheduledTask):
        task = ScheduledTask.objects.get(id=task)

    # Check if update was called because of failed task
    if is_failed_task:
        task.failed_attempts = task.failed_attempts + 1
        task.next_run = (
            next_run or
            timezone.now() + timedelta(days=getattr(settings, "TASKQUEUE_RESCHEDULE_FAILED_DAYS", 1))
        )
        task.mark_for_queue = False
        task.queued = False
        task.result = "FAILED"
    else:
        # If there is a new schedule - update it
        if next_run:
            task.next_run = next_run

        if cron_schedule:
            task.cron_schedule = cron_schedule

        task.failed_attempts = 0
        task.target_queue = target_queue

    task.save()

    return task


def remove_scheduled_task(task_id=None, task_name=None, args=None):
    # Check if remove task was called with task_id - shouldn't usually happen as part of the code -
    # will be mostly used while developing
    if task_id:
        task = ScheduledTask.objects.filter(
            id=task_id, mark_for_queue=False, queued=False
        ).first()
    else:
        # Looking for a task by task name and arguments, also checking the task wasn't already queued.
        task = ScheduledTask.objects.filter(
            task_name=task_name,
            task_args=args,
            mark_for_queue=False,
            queued=False,
        ).first()

    # If the task exists and wasn't queued yet, delete it
    if task:
        task.delete()


def reschedule_task(task, result):
    # Check if the task is re-occuring task and needs to be rescheduled
    if task.cron_schedule:
        task.mark_for_queue = False
        task.queued = False
        task.failed_attempts = 0
        task.next_run = croniter(task.cron_schedule, task.next_run).get_next(datetime)

    task.result = result
    task.save()

    return task


def queue_task(task_id):
    import traceback

    task = ScheduledTask.objects.get(id=task_id)

    # Get the function that needs to be queued
    modulename, funcname = task.task_name.rsplit(".", 1)
    module = importlib.import_module(modulename)
    function = getattr(module, funcname)

    try:
        task_args = _convert_args_to_new_format(task.task_args or (json.loads(task.args or "[]")))
        task_args["kwargs"]["schedule_id"] = task_id

        task.queued = True
        task.save()

        # Enqueue the task
        function.apply_async(args=task_args["args"], kwargs=task_args["kwargs"], queue=task.target_queue)
    except Exception as e:
        fe = traceback.format_exc()
        task.result = f"{e}\n{fe}"
        task.mark_for_queue = False
        task.save()

    return task


def queue_scheduled_tasks():
    try:
        # Get all tasks that needs to be enqueued and mark them for queue
        tasks_to_queue = []
        with transaction.atomic():
            tasks_to_queue = list(
                ScheduledTask.objects.filter(
                    mark_for_queue=False, next_run__lte=timezone.now()
                ).values_list("id", flat=True)
            )
            ScheduledTask.objects.filter(id__in=tasks_to_queue).update(
                mark_for_queue=True
            )
    except:
        pass
    else:
        for task_id in tasks_to_queue:
            queue_task(task_id)


def notify_unresponsive_tasks():
    """
        This utility will run to check if there are any unresponsive tasks.
        An unresponsive task is a task that was scheduled to run more than 15 mins ago,
        is marked for queue and wasn't moved to the completed tasks table.
        In this case we will resend this task to the queue.
    """

    unresponsive_tasks = list(
        ScheduledTask.objects.filter(
            next_run__lte=timezone.now() - timedelta(
                minutes=getattr(settings, 'TASKQUEUE_NOTIFICATION_UNRESPONSIVE_TIME', 5)
            )
        ).values_list("id", flat=True)
    )

    if len(unresponsive_tasks) > 0:
        message = (
            f"{getattr(settings, 'TASKQUEUE_NOTIFICATION_MAIL_PREFIX', '')} - Some of the scheduled tasks are unresponsive. Please check the tasks with these id's %s"
            % str(unresponsive_tasks)
        )

        mail_admins(
            subject="Celery - Scheduled Tasks are unresponsive",
            message=message,
            html_message=message
        )
