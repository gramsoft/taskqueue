from django.core.management.base import BaseCommand

from taskqueue.utils import queue_scheduled_tasks


class Command(BaseCommand):
    help = "Can be run as a cronjob or directly to send queued tasks."

    def handle(self, *args, **options):
        # Send to queue all scheduled tasks
        queue_scheduled_tasks()
