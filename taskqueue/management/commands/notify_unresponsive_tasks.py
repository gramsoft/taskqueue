from django.core.management.base import BaseCommand
from taskqueue.utils import notify_unresponsive_tasks


class Command(BaseCommand):
    help = "Can be run as a cronjob or directly to check if there are any unresponsive tasks and notify the admins " \
           "about them. "

    def handle(self, *args, **options):
        # Send to queue all scheduled tasks
        notify_unresponsive_tasks()
