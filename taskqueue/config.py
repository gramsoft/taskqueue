from django.conf import settings
from kombu import Queue

_default_broker_options = {
    "polling_interval": 30.0,
    "visibility_timeout": 1800,
    "wait_time_seconds": 20,
} if settings.DEBUG else {}

_broker_settings = getattr(
    settings,
    "TASKQUEUE_BROKER_CONFIG",
    {
        "url": "amqp://",
        "options": {}
    }
)


broker_url = _broker_settings.get("url", "amqp://")
broker_transport_options = {**_default_broker_options, **_broker_settings.get("options", {})}

result_backend = getattr(settings, "TASKQUEUE_RESULT_BACKEND", "redis://localhost/0")

_queues = getattr(settings, "TASKQUEUE_QUEUES_MAPPING", {"default": "default_queue"})
task_default_queue = _queues.get("default")
task_queues = (
    Queue(q) for q in _queues.values()
)

worker_enable_remote_control = False
accept_content = ["json"]
task_serializer = "json"
worker_prefetch_multiplier = 4
task_acks_late = True
task_ignore_result = True
