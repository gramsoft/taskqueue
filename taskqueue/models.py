from django.db import models
from django.db.models.fields import (
    CharField,
    TextField,
    BooleanField,
    DateTimeField,
    IntegerField,
    FloatField,
)
from django.db.models import JSONField
from django.core.serializers.json import DjangoJSONEncoder
from taskqueue.consts import TASK_STATUSES


class ScheduledTask(models.Model):
    task_name = CharField(max_length=250)
    args = TextField(null=True)
    task_args = JSONField(null=True, blank=True, encoder=DjangoJSONEncoder)
    next_run = DateTimeField()
    cron_schedule = CharField(max_length=250, null=True, blank=True)
    mark_for_queue = BooleanField(default=False)
    queued = BooleanField(default=False)
    result = TextField(null=True)
    failed_attempts = IntegerField(default=0)
    obj_id = CharField(max_length=256, null=True, blank=True)
    target_queue = CharField(max_length=300, null=True, blank=True)


class CompletedTask(models.Model):
    task_name = CharField(max_length=250)
    args = TextField(null=True)
    task_args = JSONField(null=True, blank=True, encoder=DjangoJSONEncoder)
    next_run = DateTimeField()
    cron_schedule = CharField(max_length=250, null=True, blank=True)
    mark_for_queue = BooleanField(default=False)
    queued = BooleanField(default=False)
    result = TextField(null=True)
    failed_attempts = IntegerField(default=0)
    obj_id = CharField(max_length=256, null=True, blank=True)


class TaskMonitorLog(models.Model):
    uuid = models.UUIDField(unique=True, primary_key=True)
    task_name = CharField(max_length=250)
    args = TextField(null=True)
    task_args = JSONField(null=True, blank=True, encoder=DjangoJSONEncoder)
    start_time = FloatField(null=True, blank=True)
    end_time = FloatField(null=True, blank=True)
    execution_time = FloatField(null=True, blank=True)
    status = models.PositiveSmallIntegerField(
        choices=TASK_STATUSES.as_tuple(),
        default=TASK_STATUSES.get(TASK_STATUSES.QUEUED),
    )
