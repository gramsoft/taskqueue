from django.contrib import admin
from taskqueue.models import ScheduledTask, CompletedTask
from taskqueue.utils import reschedule_task, queue_task


class HasFailedAttempts(admin.SimpleListFilter):
    title = "Failed"

    parameter_name = "failed"

    def lookups(self, request, model_admin):
        return ("0", "No"), ("1", "Yes")

    def queryset(self, request, queryset):
        if self.value() == "0":
            return queryset.filter(failed_attempts=0)

        if self.value() == "1":
            return queryset.filter(failed_attempts__gt=0)


class ScheduledTaskAdmin(admin.ModelAdmin):
    search_fields = ["task_name"]
    list_display = (
        "task_name",
        "task_args",
        "next_run",
        "cron_schedule",
        "mark_for_queue",
        "queued",
        "result",
        "failed_attempts",
    )
    list_filter = ["task_name", "queued", "mark_for_queue", HasFailedAttempts]
    actions = ["reschedule_task", "queue_task"]

    def reschedule_task(self, request, queryset):
        for task in queryset:
            reschedule_task(task, "MANUAL RESCHEDULE")

    def queue_task(self, request, queryset):
        for task in queryset:
            queue_task(task.id)


class CompletedTaskAdmin(ScheduledTaskAdmin):
    actions = []


admin.site.register(CompletedTask, CompletedTaskAdmin)
admin.site.register(ScheduledTask, ScheduledTaskAdmin)
